package com.example.edt34f;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    ListView listView;
    List<Item> items = new ArrayList<>();

    CustomAdapter customAdapter = new CustomAdapter(this, items);

    public static String POSITION_GLOBAL = "com.example.edt34f.POSITION_GLOBAL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //Hooks
        listView = findViewById(R.id.listView);

        listView.setAdapter(customAdapter);

        Item item1 = new Item(R.drawable.foto1, "Refugi Josep Maria Blanc", "Parc Aigüestortes", "Vall d'Aran", "Catalunya", "Distància: 2:30h", "Desnivell: 1200m", "2.100m", "30", "Open");
        Item item2 = new Item(R.drawable.foto2, "Refugi Cap de Llauset", "Posets-Maladeta", "Osca", "Aragó", "Distància: 2:15h", "Desnivell: 1100m", "2.800m", "25", "Open");
        Item item3 = new Item(R.drawable.foto3, "Refugi Ventosa i Clavell", "Parc Aigüestortes", "Vall d'Aran", "Catalunya", "Distància: 3:15h", "Desnivell: 800m", "2.150m", "45", "Closed");
        Item item4 = new Item(R.drawable.foto4, "Refugi Amitges", "Parc Aigüestortes", "Vall d'Aran", "Catalunya", "Distància: 2:30h", "Desnivell: 750m", "2.400m", "87", "Open");
        Item item5 = new Item(R.drawable.foto5, "Refugi Josep Maria Montfort", "Alt Pirineu", "Vall Ferrera", "Catalunya", "Distància: 2:30h", "Desnivell: 950m", "1.875m", "23", "Open");

        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);
        items.add(item5);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayInfo(position);
            }
        });

    }

    private class CustomAdapter extends BaseAdapter {
        private Context context;
        private List<Item> items;

        public CustomAdapter(Context context, List<Item> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data, null);

            ImageView imageRow = view.findViewById(R.id.imageRow);
            TextView titleRow = view.findViewById(R.id.titleRow);
            TextView placeRow = view.findViewById(R.id.placeRow);
            TextView cityRow = view.findViewById(R.id.cityRow);
            TextView regionRow = view.findViewById(R.id.regionRow);
            TextView distanceRow = view.findViewById(R.id.distanceRow);
            TextView desnivellRow = view.findViewById(R.id.desnivellRow);
            TextView mRow = view.findViewById(R.id.mRow);
            TextView carRow = view.findViewById(R.id.carRow);
            TextView statusRow = view.findViewById(R.id.statusRow);

            imageRow.setImageResource(items.get(position).getImageRow());
            titleRow.setText(items.get(position).getTitleRow());
            placeRow.setText(items.get(position).getPlaceRow());
            cityRow.setText(items.get(position).getCityRow());
            regionRow.setText(items.get(position).getRegionRow());
            distanceRow.setText(items.get(position).getDistanceRow());
            desnivellRow.setText(items.get(position).getDesnivellRow());
            mRow.setText(items.get(position).getmRow());
            carRow.setText(items.get(position).getCarRow());
            statusRow.setText(items.get(position).getStatusRow());

            return view;
        }
    }

    private void displayInfo(int position) {
        Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
        intent.putExtra(POSITION_GLOBAL, String.valueOf(position));
        startActivity(intent);
    }
}
