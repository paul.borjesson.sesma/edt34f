package com.example.edt34f;

public class Item {

    private int imageRow;
    private String titleRow;
    private String placeRow;
    private String cityRow;
    private String regionRow;
    private String distanceRow;
    private String desnivellRow;
    private String mRow;
    private String carRow;
    private String statusRow;

    public Item(int imageRow, String titleRow, String placeRow, String cityRow, String regionRow, String distanceRow, String desnivellRow, String mRow, String carRow, String statusRow) {
        this.imageRow = imageRow;
        this.titleRow = titleRow;
        this.placeRow = placeRow;
        this.cityRow = cityRow;
        this.regionRow = regionRow;
        this.distanceRow = distanceRow;
        this.desnivellRow = desnivellRow;
        this.mRow = mRow;
        this.carRow = carRow;
        this.statusRow = statusRow;
    }

    public int getImageRow() {
        return imageRow;
    }

    public void setImageRow(int imageRow) {
        this.imageRow = imageRow;
    }

    public String getTitleRow() {
        return titleRow;
    }

    public void setTitleRow(String titleRow) {
        this.titleRow = titleRow;
    }

    public String getPlaceRow() {
        return placeRow;
    }

    public void setPlaceRow(String placeRow) {
        this.placeRow = placeRow;
    }

    public String getCityRow() {
        return cityRow;
    }

    public void setCityRow(String cityRow) {
        this.cityRow = cityRow;
    }

    public String getRegionRow() {
        return regionRow;
    }

    public void setRegionRow(String regionRow) {
        this.regionRow = regionRow;
    }

    public String getDistanceRow() {
        return distanceRow;
    }

    public void setDistanceRow(String distanceRow) {
        this.distanceRow = distanceRow;
    }

    public String getDesnivellRow() {
        return desnivellRow;
    }

    public void setDesnivellRow(String desnivellRow) {
        this.desnivellRow = desnivellRow;
    }

    public String getmRow() {
        return mRow;
    }

    public void setmRow(String mRow) {
        this.mRow = mRow;
    }

    public String getCarRow() {
        return carRow;
    }

    public void setCarRow(String carRow) {
        this.carRow = carRow;
    }

    public String getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(String statusRow) {
        this.statusRow = statusRow;
    }
}
