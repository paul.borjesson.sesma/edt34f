package com.example.edt34f;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private ImageView imageView2;
    private TextView textView;
    private TextView textView2;

    AnimatorSet a = new AnimatorSet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        imageView = findViewById(R.id.imageView);
        imageView2 = findViewById(R.id.imageView2);
        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        ObjectAnimator objectAnimator = new ObjectAnimator();
        objectAnimator = ObjectAnimator.ofFloat(imageView2, "alpha", 0, 1).setDuration(2000);
        objectAnimator.setStartDelay(2000);

        a.playTogether(
                ObjectAnimator.ofFloat(imageView, "translationX", imageView.getX() - 400, imageView.getX()).setDuration(2000),
                ObjectAnimator.ofFloat(imageView, "alpha", 0, 1).setDuration(2000),
                ObjectAnimator.ofFloat(textView, "translationX", imageView.getX() + 400, imageView.getX()).setDuration(2000),
                ObjectAnimator.ofFloat(textView, "alpha", 0, 1).setDuration(2000),
                ObjectAnimator.ofFloat(textView2, "translationX", imageView.getX() + 400, imageView.getX()).setDuration(2000),
                ObjectAnimator.ofFloat(textView2, "alpha", 0, 1).setDuration(2000),
                objectAnimator
        );
        a.start();

        a.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(imageView, "imageApp");
                pairs[1] = new Pair<View, String>(textView, "textApp");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                    startActivity(intent, options.toBundle());
                }
            }
        });
    }

}