package com.example.edt34f;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservaActivity extends AppCompatActivity {

    Spinner spinner;
    ImageView imagePlace;

    Switch switch1;
    RadioGroup rg1;
    RadioButton rb1;
    RadioButton rb2;
    RadioButton rb3;
    RadioButton rb4;

    DatePickerDialog picker1;
    EditText textDate;
    DatePickerDialog picker2;
    EditText textDate2;

    Button btnReservar;
    TextInputLayout til1;
    TextInputLayout til2;
    TextInputLayout til3;
    TextInputLayout til4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);
        getSupportActionBar().hide();

        //Hooks
        spinner = findViewById(R.id.spinner);
        imagePlace = findViewById(R.id.imagePlace);
        switch1 = findViewById(R.id.switch1);
        rg1 = findViewById(R.id.radioGroup1);
        rb1 = findViewById(R.id.radioButton);
        rb2 = findViewById(R.id.radioButton2);
        rb3 = findViewById(R.id.radioButton3);
        rb4 = findViewById(R.id.radioButton4);
        textDate = (EditText) findViewById(R.id.textEntrada);
        textDate2 = (EditText) findViewById(R.id.textSortida);
        btnReservar = findViewById(R.id.btnReservar);
        til1 = findViewById(R.id.til1);
        til2 = findViewById(R.id.til2);
        til3 = findViewById(R.id.til3);
        til4 = findViewById(R.id.til4);

        //Botó reservar amb compartir al mail
        btnReservar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:")).setType("text/plain");
                String[] TO = {"paul.borjesson.sesma3435@gmail.com"};
                String[] CC = {"paul.borjesson.sesma@gmail.com"};

                String EXTRA_NOM = til1.getEditText().getText().toString();
                String EXTRA_COGNOMS = til2.getEditText().getText().toString();
                String EXTRA_REFUGI = spinner.getSelectedItem().toString();
                String EXTRA_EMAIL = til3.getEditText().getText().toString();
                String EXTRA_NUM_PERSONES = til4.getEditText().getText().toString();
                String EXTRA_PENSIO_COMPLETA;
                if (switch1.isChecked()) {EXTRA_PENSIO_COMPLETA = "SÍ";} else {EXTRA_PENSIO_COMPLETA = "NO";}
                String EXTRA_MENU;
                int radioSelected = rg1.getCheckedRadioButtonId();
                RadioButton radioButton = findViewById(radioSelected);
                EXTRA_MENU = radioButton.getText() + "";
                String EXTRA_ENTRADA = textDate.getText().toString();
                String EXTRA_SORTIDA = textDate2.getText().toString();

                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "RESERVA PER APP");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Reserva per App\n\nNom: " + EXTRA_NOM + " " + EXTRA_COGNOMS + "\nRefugi: " + EXTRA_REFUGI + "\nEntrada: "
                        + EXTRA_ENTRADA + "\nSortida: " + EXTRA_SORTIDA + "\nEmail: " + EXTRA_EMAIL + "\nNúmero de persones: " + EXTRA_NUM_PERSONES + "\nMenú: " + EXTRA_MENU);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ReservaActivity.this, "There is no email client installed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        //DatePicker
        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                picker1 = new DatePickerDialog(ReservaActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                textDate.setText(day + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker1.show();
            }
        });

        textDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                picker2 = new DatePickerDialog(ReservaActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                textDate2.setText(day + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker2.show();
            }
        });

        //RadioGroup & Switch
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb1.setClickable(true);
                    rb2.setClickable(true);
                    rb3.setClickable(true);
                } else {
                    rb4.setChecked(true);
                    rb1.setChecked(false);
                    rb2.setChecked(false);
                    rb3.setChecked(false);
                    rb1.setClickable(false);
                    rb2.setClickable(false);
                    rb3.setClickable(false);
                }
            }
        });

        //Spinner
        List<String> placesList = new ArrayList<>();
        placesList.add("Refugi Josep Maria Blanc");
        placesList.add("Refugi Cap de Llauset");
        placesList.add("Refugi Ventosa i Clavell");
        placesList.add("Refugi Amitges");
        placesList.add("Refugi Josep Maria Montfort");

        List<Drawable> placesListImg = new ArrayList<>();
        placesListImg.add(ResourcesCompat.getDrawable(getResources(), R.drawable.foto1, null));
        placesListImg.add(ResourcesCompat.getDrawable(getResources(), R.drawable.foto2, null));
        placesListImg.add(ResourcesCompat.getDrawable(getResources(), R.drawable.foto3, null));
        placesListImg.add(ResourcesCompat.getDrawable(getResources(), R.drawable.foto4, null));
        placesListImg.add(ResourcesCompat.getDrawable(getResources(), R.drawable.foto5, null));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
            this,
                R.layout.support_simple_spinner_dropdown_item,
                placesList
        );

        spinner.setAdapter(adapter);

        //Intent so things update on click from dashboard
        Intent intent = getIntent();
        int pos = Integer.parseInt(intent.getStringExtra(DashboardActivity.POSITION_GLOBAL));
        imagePlace.setImageDrawable(placesListImg.get(pos));
        spinner.setSelection(pos);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                imagePlace.setImageDrawable(placesListImg.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "NOTHING SELECTED", Toast.LENGTH_LONG).show();
            }
        });

    }
}